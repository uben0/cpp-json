/*
	Gives run time check of json value integrity.

		- If the model is Null, the instance is accepted no matter its value.

		- If the model is a Boolean, Number or a String, the instance is
		accepted if it is of the same type, however its value does not matter.

		- If the model is an empty Array, the instance is accepted if if it is
		also an Array, however its content does not matter.

		- If the model is an non empty Array, the instance is accepted if it is
		also an Array and if all its values match at least one of the model
		values.

		- If the model is an empty Object, the instance is accepet if it is also
		an Object, however its content does not matter.
		
		- If the model is an non empty Object, the instance is accepted if it is
		also an Object and if each properties of the model are present is the
		instance and each of theme has to match with their counterpart. If the
		model has a propertie named with an empty String, it is not checked like
		others but all of the instance properties has to match it.
*/


bool Json::match(const Json& model) const {
	switch (model.m_type) {
		case TYPE_OBJECT: {
		
			if (not is_object()) {
				return false;
			}
			if (model.object().size() == 0) {
				return true;
			}
			
			for (
				Object::const_iterator iterator1 = model.object().begin();
				iterator1 != model.object().end();
				iterator1++
			) {
			
				if (iterator1->first == "") {
					for (
						Object::const_iterator iterator2 = object().begin();
						iterator2 != object().end();
						iterator2++
					) {
						if (not iterator2->second.match(iterator1->second)) {
							return false;
						}
					}
				}
				else {
					Object::const_iterator iterator2;
					for (
						iterator2 = object().begin();
						iterator2 != object().end();
						iterator2++
					) {
						if (iterator1->first == iterator2->first) break;
					}
					if (
						iterator2 == object().end() or
						not iterator2->second.match(iterator1->second)
					) return false;
				}
			}
			return true;
		}
		break;
		case TYPE_ARRAY: {
			
			if (not is_array()) {
				return false;
			}
			if (model.array().size() == 0) {
				return true;
			}
			
			for (
				Array::const_iterator iterator1 = array().begin();
				iterator1 != array().end();
				iterator1++
			) {
				Array::const_iterator iterator2;
				for (
					iterator2 = model.array().begin();
					iterator2 != model.array().end();
					iterator2++
				) {
					if (iterator1->match(*iterator2)) break;
				}
				if (iterator2 == model.array().end()) {
					return false;
				}
			}
			return true;
		}
		break;
		case TYPE_NULL: return true;
		default: return model.type() == type();
	}
	return false;
}

