/*
	Gives direct access to contained element.s when the json value is an array
	or an object. For instance, 'json.object()["key"]' becomes 'json["key"]'.
*/

Json& Json::operator[](unsigned int index) {
	return array()[index];
}

const Json& Json::operator[](unsigned int index) const {
	return array().at(index);
}

Json& Json::operator[](const String& key) {
	return object()[key];
}

const Json& Json::operator[](const String& key) const {
	return object().at(key);
}

