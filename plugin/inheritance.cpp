/*
	Make class Json inherit from these two classes, then any other classes
	inheriting from theme can be used identically in printing and parsing
	processes.

	Modify Json class definition to inherit from these:
	'class Json: public JsonParsable, public JsonPrintable'
*/

class JsonParsable {
public:
	virtual void parse(std::istream& istream) = 0;
};

class JsonPrintable {
public:
	virtual void print(
		std::ostream& ostream,
		bool pretty = false,
		unsigned int indent = 0
	) const = 0;
};


std::istream& operator>>(std::istream& istream, JsonParsable& dest) {
	dest.parse(istream);
	return istream;
}

std::ostream& operator<<(std::ostream& ostream, const JsonPrintable& src) {
	src.print(ostream);
	return ostream;
}

