# cpp-json
## Introduction
A simple c++ json implementation. It introduces a **Json class** representing a
json value.<br>It supports **parsing** json code to Json class and **printing**
json code from Json class.

A Json value can be one of the following type:

| Json type       | C++ type                 |
|-----------------|--------------------------|
| `Json::Null`    | `Json`                   |
| `Json::Boolean` | `bool`                   |
| `Json::Number`  | `long int`               |
| `Json::String`  | `std::string`            |
| `Json::Array`   | `std::vector<Json>`      |
| `Json::Object`  | `std::map<String, Json>` |

These are `typedef`, they are equivalent.

## Initialize
Initialize a Json variable by giving the constructor a Json value:
+ `Json json(Json::Null());`
+ `Json json(Json::Boolean(true));`
+ `Json json(Json::Integer(4321));`
+ `Json json(Json::String("hello world"));`
+ `Json json(Json::Array({Json(), ...}));`
+ `Json json(Json::Object({{"keyName", Json()}, ...}));`

The assignation and the copy are also implemented:
+ `Json json(Json());`
+ `Json json = Json::TypeName(typeValue));`
+ `Json json = Json(Json::TypeName(typeValue));`

## Parse and print
Two methods make parsing and printing possible:
+ `json.parse(istream);`
+ `json.print(ostream);`

## Access the value
An accessor makes value manipulation possible:
+ `Boolean& Json::boolean();`
+ `Number& Json::number();`
+ `String& Json::string();`
+ `Array& Json::array();`
+ `Object& Json::object();`

Returns a reference to the held value, throw a `bad_type` exception if the requested type is not the same as the stored type.

## Example
```c++
#include <fstream>
#include "json.hpp"

int main() {
    Json json = Json::Object({
    	{"width", Json::Number(1920)},
    	{"height", Json::Number(1080)},
    	{"profiles", Json::Array({
    		Json::Object({
    			{"name", Json::String("default")},
    			{"administrator", Json::Boolean(true)},
    			{"settings", Json::Null()}
    		})
    	})}
    });

    std::ifstream file("settings.json");

    json.object()["profiles"].array()[0].object()["settings"].parse(file);

    std::cout << json << std::endl;

    return EXIT_SUCCESS;
}
```

## Plugins
### Direct access (Array and Object)

Quick Array and Object access can be added:
+ `json[16]` equivalent to `json.array()[16]`
+ `json["key"]` equivalent to `json.object()["key"]`

### Match instance to model
A match method can be added to provide run time integrity check for json value:
+ If the model is Null, the instance is accepted no matter
its value.
+ If the model is a Boolean, Number or a String, the
instance is accepted if it is of the same type, however
its value does not matter.
+ If the model is an empty Array, the instance is accepted
if it is also an Array, however its content does not
matter.
+ If the model is an non empty Array, the instance is
accepted if it is also an Array and if all its values
match at least one of the model values.
+ If the model is an empty Object, the instance is accepet
if it is also an Object, however its content does not
matter.
+ If the model is an non empty Object, the instance is
accepted if it is also an Object and if each properties
of the model are present is the instance and each of
theme has to match with their counterpart. If the model
has a propertie named with an empty String, it is not
checked like others but all of the instance properties
has to match it.

For instance:
+ `json.match(Json::Array({Json::Boolean()}))` returns `true` if `json` is an
Array of Boolean.
+ `json.match(Json::Object({{"", Json::Number()}}))` returns `true` if `json` is
an Object with properties that only hold Number.

### Inherit to print and parse non json class

A `JsonParsable` class and `JsonPrintable` class can be added to make non json class
convertible from json code and to json.

## RFC 8259 compatibility
The default implementation does not respect the
[RFC 8258](https://tools.ietf.org/html/rfc8259). But an adaptation has been made
to match it. It changes `Object` type from `std::map` to `std::vector` and split
`Number` between `Integer` and `Floating`. Find it in the 'rfc8259' directory.
